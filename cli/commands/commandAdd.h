#ifndef COMMAND_ADD_H
#define COMMAND_ADD_H
#include "stringTools/strTools.h"
#include <time.h>


size_t add_input_item(FILE *f, char **str, size_t pattern_size, const uint32_t *pattern_types, char **pattern_names);

#endif
