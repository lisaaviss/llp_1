#ifndef COMMAND_FIND_H
#define COMMAND_FIND_H
#include "../../core/crudInter.h"
#include <stdbool.h>
#include "stringTools/strTools.h"

void
find_by(FILE *f, char **arr, size_t pattern_size, const uint32_t *pattern_types, char **pattern_names, size_t count);

#endif
