#ifndef COMMAND_UPD_H
#define COMMAND_UPD_H

#include "stringTools/strTools.h"

size_t update_item(FILE *f, char **str, size_t pattern_size, const uint32_t *pattern_types, char **pattern_names, size_t fields_count);

#endif