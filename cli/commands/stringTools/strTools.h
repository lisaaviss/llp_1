#ifndef STR_TOOLS_H
#define STR_TOOLS_H
#include "../../../core/crudInter.h"
#include <stdbool.h>
#include "../commandAdd.h"

char *concat(const char *s1, const char *s2);
bool isNumeric(const char *str);
size_t split(char *str, char c, char ***arr);
void parse_file(FILE *to, FILE *from);

#endif
