#ifndef INTERACTIVE_H
#define INTERACTIVE_H

#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include "../core/crudInter.h"
#include <stdbool.h>
#include "commands/commandAdd.h"
#include "commands/commandFind.h"
#include "commands/commandHelp.h"
#include "commands/commandUpd.h"


void interactive_mode(FILE* f);


#endif
